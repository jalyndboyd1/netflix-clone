import React, { useState, useEffect } from "react";
import "./Banner.css";
import axios from "./axios";
import requests from "./request";

function Banner() {
  const [movie, setMovie] = useState([]);
  const [fadeIn, setFadeIn] = useState(true);

  useEffect(() => {
    async function fetchData() {
      const request = await axios.get(requests.fetchNetflixOriginals);
      setMovie(
        request.data.results[
          Math.floor(Math.random() * request.data.results.length - 1)
        ]
      );
      const interval = setInterval(() => {
        setMovie(
          request.data.results[
            Math.floor(Math.random() * request.data.results.length - 1)
          ]
        );
        // console.log(movie);
        return () => clearInterval(interval);
      }, 6000);
      return request;
    }
    fetchData();
  }, []);

  useEffect(() => {
    const fadeInterval = setInterval(() => {
      setFadeIn(!fadeIn);
    }, 6000);
    return () => clearInterval(fadeInterval);
  }, [fadeIn]);

  function truncate(str, num) {
    return str?.length > num ? str.substr(0, num - 1) + "..." : str;
  }
  return (
    <header
      className={fadeIn ? "banner" : "title-fade-out"}
      style={{
        backgroundSize: "cover",
        backgroundImage: `url(
            "https://image.tmdb.org/t/p/original/${movie?.backdrop_path}"
        )`,
        backgroundPosition: "center center",
      }}
    >
      <div className="banner-contents">
        <h1 className="banner-title">
          {movie?.title || movie?.name || movie?.original_name}
        </h1>
        <div className="banner-buttons">
          <button className="banner-button">Play</button>
          <button className="banner-button">My List</button>
          <h1 className="movie-overview">{truncate(movie?.overview, 100)}</h1>
        </div>
      </div>
      <div className="fade-bottom" />
    </header>
  );
}

export default Banner;
