import React, { useEffect, useState } from "react";
import "./Nav.css";
import icon from "./netflix-icon.png";

function Nav() {
  const [show, handleShow] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 100) {
        // show true
        handleShow(true);
      } else {
        handleShow(false);
      }
      return () => {
        console.log("removed");
        window.removeEventListener("scroll");
      };
    });
  }, []);

  return (
    <div className={`nav ${show && "nav-black"}`}>
      <img
        src="https://www.freepnglogos.com/uploads/netflix-logo-0.png"
        alt="Netflix"
        className="nav-logo"
      />
      <img src={icon} alt="Profile Icon" className="nav-icon" />
    </div>
  );
}

export default Nav;
