export const initialState = {
  playing: false,
  trailerUrl: "",
};

export const SET_PLAYING = "SET_PLAYING";
// export const SET_URL = "SET_URL";

const reducer = (state, action) => {
  switch (action.type) {
    // case SET_URL:
    //   return {
    //     ...state,
    //     trailerUrl: action.trailerUrl,
    //   };
    case SET_PLAYING:
      return {
        ...state,
        playing: action.playing,
      };

    default:
      return state;
  }
};
export default reducer;
