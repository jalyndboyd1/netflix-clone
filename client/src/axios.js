import axios from "axios";

const instance = axios.create({
  baseURL: "http://api.themoviedb.org/3",
});

// instance.get("") adds on as a param to the end of req

export default instance;
